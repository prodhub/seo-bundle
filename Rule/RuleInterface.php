<?php

namespace ADW\SEOBundle\Rule;

use ADW\SEOBundle\Metadata\MetaTagInterface;

/**
 * Interface RuleInterface.
 */
interface RuleInterface
{
    /**
     * @return string
     */
    public function getPattern();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return MetaTagInterface[]
     */
    public function getMetaTags();

    /**
     * @return array
     */
    public function getExtra();
}
