<?php

namespace ADW\SEOBundle\Rule;

use Doctrine\ORM\EntityRepository;

/**
 * Class DoctrineRulesProvider.
 *
 * @author ADW
 */
class DoctrineRulesProvider implements RulesProviderInterface
{
    /**
     * @var EntityRepository
     */
    protected $em;

    /**
     * @param EntityRepository $repository
     */
    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function load()
    {
        return $this->repository->findBy([], ['priority' => 'DESC']);
    }
}
