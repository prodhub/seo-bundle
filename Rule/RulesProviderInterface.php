<?php

namespace ADW\SEOBundle\Rule;

/**
 * Interface RulesProviderInterface.
 */
interface RulesProviderInterface
{
    /**
     * @return RuleInterface[]
     */
    public function load();
}
