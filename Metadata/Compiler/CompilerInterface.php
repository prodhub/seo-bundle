<?php

namespace ADW\SEOBundle\Metadata\Compiler;

use ADW\SEOBundle\Metadata\CompiledMetadata;
use ADW\SEOBundle\Rule\RuleInterface;

/**
 * Interface CompilerInterface.
 */
interface CompilerInterface
{
    /**
     * @param RuleInterface $rule
     * @param array         $context
     *
     * @return CompiledMetadata
     */
    public function compileMetadata(RuleInterface $rule, array $context);
}
