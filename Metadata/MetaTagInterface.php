<?php

namespace ADW\SEOBundle\Metadata;

/**
 * Interface MetaTagInterface.
 */
interface MetaTagInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getProperty();

    /**
     * @return string
     */
    public function getContent();
}
