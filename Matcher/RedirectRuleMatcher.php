<?php

namespace ADW\SEOBundle\Matcher;

use ADW\SEOBundle\Matcher\SyntaxHandler\AsteriskHandler;
use ADW\SEOBundle\Matcher\SyntaxHandler\SyntaxHandlerInterface;
use ADW\SEOBundle\RedirectRule\RedirectRuleInterface;

/**
 * Class RedirectRuleMatcher.
 *
 * @author ADW
 *
 * TODO: maybe need optimization
 * TODO: tagged syntax handlers
 */
class RedirectRuleMatcher
{
    /**
     * @var SyntaxHandlerInterface[]
     */
    protected $syntaxHandlers = [];

    public function __construct()
    {
        $this->syntaxHandlers[] = new AsteriskHandler();
    }

    /**
     * @param string                  $path
     * @param RedirectRuleInterface[] $redirectRulesCollection
     *
     * @return null|RedirectRuleInterface
     */
    public function match($path, $redirectRulesCollection)
    {
        foreach ($redirectRulesCollection as $rule) {
            $pattern = preg_quote($rule->getSourceTemplate(), '~');

            foreach ($this->syntaxHandlers as $handler) {
                $pattern = $handler->handle($pattern);
            }

            $pattern = '~\A'.$pattern.'\z~';

            if (preg_match($pattern, $path)) {
                return $rule;
            }
        }

        return null;
    }
}
