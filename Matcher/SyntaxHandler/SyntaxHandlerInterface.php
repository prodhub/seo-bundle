<?php

namespace ADW\SEOBundle\Matcher\SyntaxHandler;

/**
 * Interface SyntaxHandlerInterface.
 */
interface SyntaxHandlerInterface
{
    /**
     * @param string $pattern
     *
     * @return string
     */
    public function handle($pattern);
}
