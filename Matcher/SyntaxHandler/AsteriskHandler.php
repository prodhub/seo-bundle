<?php

namespace ADW\SEOBundle\Matcher\SyntaxHandler;

/**
 * Class AsteriskHandler.
 *
 * @author ADW
 */
class AsteriskHandler implements SyntaxHandlerInterface
{
    /**
     * @param string $pattern
     *
     * @return string
     */
    public function handle($pattern)
    {
        return str_replace('\{\*\}', '.*', $pattern);
    }
}
