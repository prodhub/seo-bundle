# SEO Bundle

Provides admin interface (sonata admin) for pages metadata and redirect rules.

Supports url templates and twig syntax for values.

## Installation
Add repository


```
"repositories": [
    {"type": "vcs", "url": "https://bitbucket.org/prodhub/seo-bundle.git"}        
]
```

```
# bash
composer require adw/seo-bundle dev-master

```

```
# AppKernel.php
$bundles = [
   new \ADW\SEOBundle\ADWSEOBundle()
];
```

```
# Twig
{% set seoData = seo_load() %}
{{ seo_title(seoData) }} # render title for current page
{{ seo_meta_tags(seoData) }} # render metatags for current page
{{ seo_extra(seoData, 'my-extra-item-key') }} # get extra data for current page
```

## Configuration (optional)
```
adwseo:
    redirects: 
      enabled: false # Redirects feature disabled by default
      not_found_only: true # Redirect listener only for NotFoundhttpException by default
    cache: my_doctrine_cache_provider_name # Cache for rules and metadata. Disabled by default
```
