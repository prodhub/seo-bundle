<?php

namespace ADW\SEOBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ADWSEOBundle.
 *
 * @author ADW
 */
class ADWSEOBundle extends Bundle
{
}
