<?php

namespace ADW\SEOBundle\RedirectRule;

use Doctrine\ORM\EntityRepository;

/**
 * Class DoctrineRedirectRuleRepository.
 *
 * @author ADW
 */
class DoctrineRedirectRuleRepository extends EntityRepository implements RedirectRuleManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllSortedByPriority()
    {
        return $this->findBy([], ['priority' => 'DESC']);
    }

    /**
     * {@inheritdoc}
     */
    public function save(RedirectRuleInterface $rule)
    {
        $em = $this->getEntityManager();
        $em->persist($rule);
        $em->flush($rule);
    }
}
