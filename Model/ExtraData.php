<?php

namespace ADW\SEOBundle\Model;

/**
 * Class ExtraData.
 *
 * @author ADW
 */
class ExtraData
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;
}
