<?php

namespace ADW\SEOBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

/**
 * Class ConfigureAdminMenuListener.
 *
 * @author ADW
 */
class ConfigureAdminMenuListener
{
    /**
     * @param ConfigureMenuEvent $event
     */
    public function configureMenu(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        if (!$seo = $menu->getChild('SEO')) {
            return;
        }

        $seo->addChild('Настройки robots', ['route' => 'adw.seo.admin.robots.edit']);
    }
}
